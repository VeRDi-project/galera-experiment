#!/usr/bin/env python
import logging
import os
import time

from experiment_utilities.concerto_g5k import ConcertoG5k
from experiment_utilities.remote_host import RemoteHost
from experiment_utilities.reserve_g5k import G5kReservation

EXP_GIT = 'https://gitlab.inria.fr/VeRDi-project/galera-experiment.git'
EXP_ROOT_IN_GIT = 'galera-experiment'
PYTHON_FILE = 'galera_assembly.py'
PYTHON_ANSIBLE_FILE = 'galera_ansible.py'

DEFAULT_WORKING_DIRECTORY = '.'


def run_concerto_experiment(nb_db_entries, conf, experiment_directory,
                            working_directory=DEFAULT_WORKING_DIRECTORY, force_deployment=True, destroy=False):
    with G5kReservation(conf, force_deployment, destroy) as g5k:
        concerto_machine = g5k.get_hosts_info(role='concerto')[0]
        database_machines = [host for host in g5k.get_hosts_info(role='database')]
        database_add_machines = [host for host in g5k.get_hosts_info(role='database_add')]
        master_machine = database_machines[0]
        workers_machines = database_machines[1:len(database_machines)]

        print("Databases: %s" % str(database_machines))
        print("Additional workers: %s" % str(database_add_machines))
        print("Concerto: %s" % str(concerto_machine))

        from copy import deepcopy
        ansible_config = deepcopy(conf)
        ansible_config['g5k']['resources']['machines'][2]['nodes'] = 0
        ansible_config["nb_db_entries"] = nb_db_entries

        concerto_config = {
            "database_hosts": database_machines,
            "master_host": master_machine,
            "workers_hosts": workers_machines,
            "additional_workers_hosts": database_add_machines,
            "concerto_host": concerto_machine,
            "nb_db_entries": nb_db_entries
        }
        with g5k.ansible_to("concerto") as ansible_to_concerto:
            ansible_to_concerto.apt(name=["python3", "python3-pip", "git", "ansible"], state="present")
            ansible_to_concerto.pip(name=["enoslib", "ansible"], executable="pip3")

        if experiment_directory == "ansible":
            # Ansible
            from ansible_g5k import AnsibleG5k
            g5k.generate_inventory("inventory.ini")
            with RemoteHost(concerto_machine["address"], remote_user="root") as ansible_host:
                with AnsibleG5k(
                        remote_host=ansible_host,
                        remote_exp_dir=EXP_ROOT_IN_GIT + "/" + experiment_directory,
                        python_file=PYTHON_ANSIBLE_FILE,
                        ansible_config=ansible_config,
                        local_wd=working_directory,
                        send_ssh_keys=True
                ) as ansible_g5k:
                    ansible_g5k.clone_git(EXP_GIT)
                    ansible_g5k.send_files([".python-grid5000.yaml"], remote_location="~", relative_to_wd=False)
                    ansible_g5k.send_files(["inventory.ini"], relative_to_wd=False)
                    ansible_g5k.execute(timeout="45m")
                    files_to_get = ['stdout', 'stderr', 'times.json', 'inventory.ini']
                    ansible_g5k.get_files(files_to_get)

        else:
            # Concerto / Aeolus
            with RemoteHost(concerto_machine["address"], remote_user="root") as concerto_host:
                with ConcertoG5k(
                        remote_host=concerto_host,
                        remote_exp_dir=EXP_ROOT_IN_GIT + "/" + experiment_directory,
                        python_file=PYTHON_FILE,
                        concerto_config=concerto_config,
                        local_wd=working_directory,
                        send_ssh_keys=True
                ) as concerto_g5k:
                    concerto_g5k.clone_git(EXP_GIT)
                    concerto_g5k.execute(timeout="45m")
                    files_to_get = ['stdout', 'stderr', 'gantt_chart.json', 'times.json', 'timeout']
                    concerto_g5k.get_files(files_to_get)
    

DEFAULT_SWEEPER_NAME = "sweeper-galera-experiments"
DEFAULT_FRAMEWORKS = ("concerto", "aeolus", "ansible")
DEFAULT_NBS_NODES = ((3, 1), (3, 5), (3, 10), (3, 20), (5, 0), (10, 0), (20, 0))
DEFAULT_NBS_ENTRIES = (1000,)
DEFAULT_START_ATTEMPT = 1
DEFAULT_NB_ATTEMPTS = 20
DEFAULT_SWEEPER_DIR = os.path.join(os.getenv('HOME'), DEFAULT_SWEEPER_NAME)


def perform_experiments(frameworks=DEFAULT_FRAMEWORKS, start_attempt=DEFAULT_START_ATTEMPT,
                        nb_attempts=DEFAULT_NB_ATTEMPTS, nbs_nodes=DEFAULT_NBS_NODES, nbs_entries=DEFAULT_NBS_ENTRIES,
                        sweeper_dir=DEFAULT_SWEEPER_DIR):
    import copy
    import yaml
    import traceback
    from os import makedirs
    from pprint import pformat
    from execo_engine.sweep import ParamSweeper, sweep

    with open("conf.yaml") as f:
        initial_config = yaml.load(f)
    
    sweeper = ParamSweeper(
        sweeper_dir,
        sweeps=sweep({
            'framework': list(frameworks),
            'nb_db_nodes': list(nbs_nodes),
            'nb_db_entries': list(nbs_entries),
            'attempt': list(range(start_attempt, start_attempt+nb_attempts))
        }))
    
    logging.info("Using Execo sweeper in directory: %s" % sweeper_dir)
    done_tasks = sweeper.get_done()
    if len(done_tasks) > 0:
        logging.info("Tasks already done:\n%s" % "\n".join("- " + pformat(task) for task in done_tasks))
    
    while sweeper.get_remaining():
        combination = sweeper.get_next()
        logging.info("Treating combination %s" % pformat(combination))

        try:
            # Setup parameters
            conf = copy.deepcopy(initial_config)  # Make a deepcopy so we can run multiple sweeps in parallels
            framework = combination['framework']
            (nb_db_nodes, nb_additional_db_nodes) = combination['nb_db_nodes']
            nb_db_entries = combination['nb_db_entries']
            attempt = combination['attempt']
            conf['g5k']['resources']['machines'][0]['nodes'] = nb_db_nodes
            conf['g5k']['resources']['machines'][1]['nodes'] = nb_additional_db_nodes
            xp_name = "%s-nb_db_%d+%d-nb_ent_%d-%d"\
                      % (framework, nb_db_nodes, nb_additional_db_nodes, nb_db_entries, attempt)

            # Let's get it started!
            wd = "exp/%s" % xp_name
            makedirs(wd, exist_ok=True)
            with open(wd+'/g5k_config.yaml', 'w') as g5k_config_file:
                yaml.dump(conf, g5k_config_file)

            run_concerto_experiment(nb_db_entries, conf, framework, working_directory=wd, destroy=False)

            # Everything works well, mark combination as done
            sweeper.done(combination)
            logging.info("End of combination %s" % pformat(combination))
            logging.info("Going to the next in 5s")
            time.sleep(5)

        except Exception as e:
            # Oh no, something goes wrong! Mark combination as canceled for
            # a later retry
            logging.error("Combination %s Failed with message \"%s\". Full exception message:\n%s"
                          % (pformat(combination), e, traceback.format_exc()))
            sweeper.cancel(combination)

        finally:
            pass


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    perform_experiments()
