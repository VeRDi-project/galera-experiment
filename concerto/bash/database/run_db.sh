chown -R 999:999 /database
docker run \
  --detach \
  --expose 4567 \
  --name mariadb-container \
  -d \
  -v /database/data:/var/lib/mysql:rw \
  -e MYSQL_INITDB_SKIP_TZINFO=yes \
  -e MYSQL_ROOT_PASSWORD=my-secret-pw \
  -p 3306:3306 \
  -p 4567:4567/udp \
  -p 4567-4568:4567-4568 \
  -p 4444:4444 \
  mariadb:10.3.14
until [ "`/usr/bin/docker inspect -f {{.State.Running}} mariadb-container`"=="true" ]; do
    sleep 0.1;
done;
