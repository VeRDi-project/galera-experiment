import time

from concerto.all import *
from concerto.utility import *
from concerto.plugins.ansible import call_ansible_on_host, AnsibleCallResult

class Docker(Component):
    
    def __init__(self, host):
        self.host = host
        self.playbook = "ansible/docker.yml"
        Component.__init__(self)

    def create(self):
        self.places = [
            'uninstalled',
            'run_mounted',
            'installed'
        ]

        self.transitions = {
            'mount_run': ('uninstalled', 'run_mounted', 'install', 0, self.mount_run),
            'use_apt_docker': ('run_mounted', 'installed', 'install', 0, empty_transition),
        }

        self.dependencies = {
            'apt_docker': (DepType.USE, ['installed']),
            'docker': (DepType.PROVIDE, ['installed'])
        }
        
        self.initial_place = 'uninstalled'
        
    def mount_run(self):
        self.print_color("Mouting /run")
        result = call_ansible_on_host(self.host["ip"], self.playbook, "docker-0", extra_vars={"enos_action":"deploy"})
        self.print_color("Mounted /run (code %d) with command: %s" % (result.return_code, result.command))
