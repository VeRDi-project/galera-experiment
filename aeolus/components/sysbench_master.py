import time

from concerto.all import *
from concerto.utility import *
from concerto.plugins.ansible import call_ansible_on_host, AnsibleCallResult

from components.cutils.remote_host import RemoteHost


class SysbenchMaster(Component):
    
    def __init__(self, host):
        self.host = host
        self.remote_host_handler = RemoteHost(host['ip'])
        Component.__init__(self)

    def create(self):
        self.places = [
            'uninstalled',
            'uninstalled_mysql',
            'user',
            'database',
            'suspended'
        ]
        
        self.groups = {
            'using_mysql': ['uninstalled_mysql', 'user', 'database']
        }

        self.transitions = {
            'use_mysql': ('uninstalled', 'uninstalled_mysql', 'install', 0, empty_transition),
            'to_user': ('uninstalled_mysql', 'user', 'install', 0, self.to_user),
            'to_database': ('user', 'database', 'install', 0, self.to_database),
            'suspend': ('database', 'suspended', 'suspend', 0, self.suspend),
            'restart': ('suspended', 'database', 'install', 1, self.restart)
        }

        self.dependencies = {
            'mysql': (DepType.USE, ['using_mysql']),
            'sysbench_db': (DepType.PROVIDE, ['database'])
        }
        
        self.initial_place = 'uninstalled'
        
    def to_user(self):
        self.print_color("Going to user with ip \'%s\'"%(self.host["ip"]))
        #time.sleep(0.9)
        self.remote_host_handler.run("bash ./create_user.sh")
        self.print_color("Create MariaDB sbtest user")

    def to_database(self):
        self.print_color("Going to database with ip \'%s\'"%self.host["ip"])
        self.remote_host_handler.run("bash ./create_database.sh")
        self.print_color("Created MariaDB sbtest database")

    def suspend(self):
        time.sleep(0)

    def restart(self):
        time.sleep(0)
