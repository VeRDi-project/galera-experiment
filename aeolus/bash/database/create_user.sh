until docker exec -i mariadb-container /usr/bin/mysql -u root --password=my-secret-pw -e "CREATE USER IF NOT EXISTS 'sbtest'@'%' IDENTIFIED BY 'sbtest';GRANT ALL PRIVILEGES ON *.* TO 'sbtest'@'%';FLUSH PRIVILEGES;"
do
  echo "Trying again in 1s"
  sleep 1;
done
