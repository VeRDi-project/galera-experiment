if [ -z "$1" ]
  then
    echo "No argument supplied, please provide G5K site"
    exit 1
fi

mkdir -p $1/bak

mv $1/figure0.pdf $1/bak/figure0.bak.pdf
mv $1/figure1.pdf $1/bak/figure1.bak.pdf
mv $1/figure2.pdf $1/bak/figure2.bak.pdf
mv $1/figure0_data.json $1/bak/figure0_data.bak.json
mv $1/figure1_data.json $1/bak/figure1_data.bak.json
mv $1/figure2_data.json $1/bak/figure2_data.bak.json
mv $1/figure0_analysis.txt $1/bak/figure0_analysis.bak.txt
mv $1/figure1_analysis.txt $1/bak/figure1_analysis.bak.txt
mv $1/figure2_analysis.txt $1/bak/figure2_analysis.bak.txt
mv $1/durations.json $1/bak/durations.bak.json
mv $1/figure1bis.pdf $1/bak/figure1bis.bak.pdf
mv $1/figure2bis.pdf $1/bak/figure2bis.bak.pdf
mv $1/figure1bis_data.json $1/bak/figure1bis_data.bak.json
mv $1/figure2bis_data.json $1/bak/figure2bis_data.bak.json

ssh $1.g5k << EOF
  cd galera-experiment/utils/generate_figures/remote/; 
  bash collect_data.sh
EOF
scp $1.g5k:galera-experiment/utils/generate_figures/remote/figure0_data.json $1/figure0_data.json
scp $1.g5k:galera-experiment/utils/generate_figures/remote/figure1_data.json $1/figure1_data.json
scp $1.g5k:galera-experiment/utils/generate_figures/remote/figure2_data.json $1/figure2_data.json
scp $1.g5k:galera-experiment/utils/generate_figures/remote/durations.json $1/durations.json

python3 json_to_figure.py $1/figure0_data.json $1/figure0 11 0 "noymin"
python3 json_to_figure.py $1/figure1_data.json $1/figure1 11 0 "noymin"
python3 json_to_figure.py $1/figure2_data.json $1/figure2 11 70
python3 get_figure1bis_data.py $1/figure1_data.json $1/durations.json > $1/figure1bis_data.json
python3 get_figure2bis_data.py $1/figure2_data.json $1/durations.json > $1/figure2bis_data.json
python3 json_to_figure.py $1/figure1bis_data.json $1/figure1bis 11 0 "noymin"
python3 json_to_figure.py $1/figure2bis_data.json $1/figure2bis 11 70
