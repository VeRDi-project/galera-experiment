for ndb in 3 10 20 50 100; do
  for nent in 1000; do
    python3 group_times_results.py "~/galera-experiment/exp/concerto-nb_db_${ndb}+*-nb_ent_${nent}-*/" total_deploy_time total_reconf_time > concerto-nb_db_${ndb}-nb_ent_${nent}.json
    python3 group_times_results.py "~/galera-experiment/exp/aeolus-nb_db_${ndb}+*-nb_ent_${nent}-*/" total_deploy_time total_reconf_time > aeolus-nb_db_${ndb}-nb_ent_${nent}.json
    python3 group_times_results.py "~/galera-experiment/exp/ansible-nb_db_${ndb}+*-nb_ent_${nent}-*/" total_deploy_time total_reconf_time > ansible-nb_db_${ndb}-nb_ent_${nent}.json
  done
done

for nadb in 1 10 20 50 100; do
  for nent in 1000; do
    python3 group_times_results.py "~/galera-experiment/exp/concerto-nb_db_3+${nadb}-nb_ent_${nent}-*/" total_reconf2_time > concerto-nb_db_3+${nadb}-nb_ent_${nent}.json
    python3 group_times_results.py "~/galera-experiment/exp/aeolus-nb_db_3+${nadb}-nb_ent_${nent}-*/" total_reconf2_time > aeolus-nb_db_3+${nadb}-nb_ent_${nent}.json
    python3 group_times_results.py "~/galera-experiment/exp/ansible-nb_db_3+${nadb}-nb_ent_${nent}-*/" total_reconf2_time > ansible-nb_db_3+${nadb}-nb_ent_${nent}.json
  done
done
