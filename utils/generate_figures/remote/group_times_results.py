import sys
import subprocess
from os.path import exists as path_exists
from json import load, dumps
import math
import statistics
from copy import deepcopy

pattern = sys.argv[1]
metrics = sys.argv[2:]

metric_dict = {
    "mean": 0.,
    "median": 0.,
    "variance": 0.,
    "standard_dev": 0.,
    "min": math.inf,
    "max": 0.,
    "points": []
}

results = {
    "nb_points": 0,
    "points": []
}

for m in metrics:
    results[m] = deepcopy(metric_dict)

try:
    command = 'ls -1d %s' % pattern
    print("Running command:%s\n" % command, file=sys.stderr)
    output = bytes.decode(subprocess.check_output(command, shell=True))
    print("Output:\n%s\n" % output, file=sys.stderr)
    folders = filter(lambda x: len(x.strip()) > 0, output.split("\n"))
except:
    folders = []
    print("No directories to process", file=sys.stderr)

for exp_folder in folders:
    # print(line)
    if not path_exists("%stimes.json" % exp_folder):
        print("Skipped %s: no time.json" % exp_folder, file=sys.stderr)
        continue
    if path_exists("%stimeout" % exp_folder):
        print("Skipped %s: timeout occurred" % exp_folder, file=sys.stderr)
        continue

    try:
        with open("%stimes.json" % exp_folder) as f:
            times = load(f)
        results["nb_points"] += 1
        results["points"].append(times)
        for m in metrics:
            time = times[m]
            results[m]["points"].append(time)

    except:
        print("Skipped %s because an exception was raised" % exp_folder, file=sys.stderr)


def compute_agregation(res_dict):
    res_dict["mean"] = statistics.mean(res_dict["points"])
    res_dict["median"] = statistics.median(res_dict["points"])
    if len(res_dict["points"]) > 1:
        res_dict["variance"] = statistics.variance(res_dict["points"], res_dict["mean"])
        res_dict["standard_dev"] = math.sqrt(res_dict["variance"])
    res_dict["min"] = min(res_dict["points"])
    res_dict["max"] = max(res_dict["points"])


if results["nb_points"] > 0:
    for m in metrics:
        compute_agregation(results[m])

print(dumps(results, indent='\t'))
