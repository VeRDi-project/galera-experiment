import logging

from reserve import perform_experiments

if __name__ == '__main__':
    # Testing
    logging.basicConfig(level=logging.DEBUG)
    perform_experiments(
        frameworks=["concerto"],
        start_attempt=1,
        nb_attempts=1,
        nbs_nodes=[(3, 1)]
    )
